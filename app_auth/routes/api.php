<?php

use App\Http\Controllers\api\GroupController;
use App\Http\Controllers\api\PermissionController;
use App\Http\Controllers\api\RoleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\UserAuthenticationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/******************************Start Authentication*********************************/
Route::group(["middleware" => "auth:api"], function () {
    Route::post("logout", [UserAuthenticationController::class, "logout"]);
    Route::post("user", [UserAuthenticationController::class, "user"]);
    /**************************Start routs that only admin can access*************************  */
    // Route::group(['middleware' => "role:super-admin"], function () {
    Route::post("users", [UserController::class, "all"]);
    Route::post("delete/user", [UserController::class, "delete"]);
    Route::post("update/user", [UserController::class, "update"]);
    Route::post("create/user", [UserController::class, "create"]);
    /**Permissions */
    Route::post("permissions", [PermissionController::class, "all"]);
    Route::post("create/permission", [PermissionController::class, "create"]);
    Route::post("delete/permission", [PermissionController::class, "delete"]);
    Route::post("assign/permission", [PermissionController::class, "assignPermission"]);
    Route::post("unassign/permission", [PermissionController::class, "unassignPermission"]);
    /**#End Permissions */
    
    /**Roles */
    Route::post("roles", [RoleController::class, "all"]);
    Route::post("create/role", [RoleController::class, "create"]);
    Route::post("delete/role", [RoleController::class, "delete"]);
    Route::post("assign/role", [RoleController::class, "assignRole"]);
    Route::post("unassign/role", [RoleController::class, "unassignRole"]);
    Route::post("assign/role/permission", [RoleController::class, "assignPermissionToRole"]);
    Route::post("unassign/role/permission", [RoleController::class, "unassignPermissionFromRole"]);
    /**#End Roles */
    // });
/**************************End routs that only admin can access*************************  */
Route::post("groups", [GroupController::class, "all"]);
Route::post("create/group", [GroupController::class, "create"]);
Route::post("invite/user", [GroupController::class, "invite"]);
Route::post("accept/user", [GroupController::class, "accept"]);
Route::post("delete/group", [GroupController::class, "delete"]);
/**************************Start routs for group controller*************************  */

/**************************End routs for group controller*************************  */

});
Route::post("login", [UserAuthenticationController::class, "login"])->name("login");
Route::post("register", [UserAuthenticationController::class, "register"]);
Route::post("verify/user", [UserAuthenticationController::class, 'verifyUser']);
Route::post("reset/password", [UserAuthenticationController::class, 'resetPassword']);
Route::post("change/password", [UserAuthenticationController::class, 'changePassword']);
Route::post("refresh/token", [UserAuthenticationController::class, 'refreshToken']);
Route::post("social/login", [UserAuthenticationController::class, 'socialLogin']);
/******************************End Authentication*************************************/
