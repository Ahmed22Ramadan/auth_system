<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Group extends Model
{
    use HasFactory;
    protected $table = "groups";
    protected $fillable = ["name","user_id","member_id"];

    /**Start realtional functions*/
    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function members(){
        return $this->belongsToMany(User::class,"groups_members","group_id","member_id");
    }
    /**End realtional functions*/
    
    /**Start Model scopes */
    public function scopeName($query, $name){
        return $this->where("name",$name);
    }
    /**End Model scopes */
}
