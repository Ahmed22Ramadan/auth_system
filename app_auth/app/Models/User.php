<?php

namespace App\Models;

use App\Models\Group;
use Laravel\Passport\HasApiTokens;
use App\Traits\HasPermissionsTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        "email_verified_at"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /****************Start Relational functions************ */
    public function linkedSocialAccounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class,"user_id");
    }

    public function invites(){
        return $this->hasMany(Invite::class,"user_id");
    }
    /****************End Relational functions************ */

    /****************Start Relational functions************ */

    public function scopeEmail($query, $email)
    {
        return $this->where("email", $email);
    }
    /****************End Scopes functions************ */

    /***********************Start Model Mutators****************** */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
    /***********************#End Model Mutators****************** */
}
