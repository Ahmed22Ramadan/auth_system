<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invite extends Model
{
    use HasFactory;
    protected $table = "invites"; 
    protected $fillable = ['email',"name",'token',"user_id"];

    /**Start relational functions */
    public function user(){
        return $this->belongsTo(User::class);
    }
    /**End relational functions */
}
