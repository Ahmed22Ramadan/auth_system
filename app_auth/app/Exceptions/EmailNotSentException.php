<?php

namespace App\Exceptions;

use App\Helpers\ApiResponse;
use Exception;

class EmailNotSentException extends Exception
{
    public function __construct(Exception $exception, ApiResponse $apiResponse)
    {
        $this->exception = $exception;
        $this->apiResponse = $apiResponse;
    }
    public function report(){

    }
    public function render($request){
            return $this->apiResponse->setError(__("api.send_email_error"))->setData($this->exception->getMessage())->getJsonResponse();
    }
}
