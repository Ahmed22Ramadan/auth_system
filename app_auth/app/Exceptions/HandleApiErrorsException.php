<?php

namespace App\Exceptions;

use Exception;
use App\Helpers\ApiResponse;
use GuzzleHttp\Exception\ClientException;

class HandleApiErrorsException extends Exception
{
    public function __construct(Exception $exception, ApiResponse $apiResponse)
    {
        $this->exception = $exception;
        $this->apiResponse = $apiResponse;
    }
    public function report(){

    }


    public function render($request){
        if($this->exception instanceof ClientException){
            return $this->apiResponse->setError(__("api.not_authorized"))->setData($this->exception->getMessage())->getJsonResponse();
        }
    }
}
