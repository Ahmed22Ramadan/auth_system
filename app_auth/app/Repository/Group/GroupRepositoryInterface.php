<?php

namespace App\Repository\Group;

interface GroupRepositoryInterface
{
    public function createInvitation(array $data);
    public function getGroupByName(string $name);
}
