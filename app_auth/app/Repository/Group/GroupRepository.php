<?php

namespace App\Repository\Group;

use App\Repository\Group\GroupRepositoryInterface;
use App\Repository\Base\BaseRepository;
use App\Models\Group;
use App\Models\Invite;

class GroupRepository extends BaseRepository implements GroupRepositoryInterface
{
    //    
    public function __construct(Group $model)
    {
        $this->mode = $model;
        parent::__construct($model);
    }

    public function createInvitation($data){
        return Invite::create($data); 
    }
    
    public function getGroupByName(string $name){
        return $this->model->name($name)->first();
    }
    
}
