<?php
namespace App\Repository\Permission;
use App\Models\Permission;
use App\Repository\Base\BaseRepository;
use App\Repository\Permission\PermissionRepositoryInterface;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    public function __construct(Permission $model)
    {
        parent::__construct($model);
    }
}