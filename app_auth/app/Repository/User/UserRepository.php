<?php

namespace App\Repository\User;

use Carbon\Carbon;
use App\Models\User;
use GuzzleHttp\Client;
use App\Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\DB;
use App\Services\SocialUserResolver;
use App\Repository\Base\BaseRepository;
use Laravel\Passport\Client as PassportClient;
use App\Repository\User\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $model, Client $client, SocialUserResolver $socialUserResolver)
    {
        parent::__construct($model);
        $this->client = $client;
        $this->socialUserResolver = $socialUserResolver;
    }

    public function getUserByEmail(string $email)
    {
         return $this->model->email($email)->first();
    }
    public function findUnexpiredUser($id)
    {
        return $this->model->where("id", $id)->where("created_at", ">", Carbon::now()->subHours(1))->first();
    }


    public function getUserByToken($token)
    {
        $rememberToken = DB::table('password_resets')
            ->where('token', $token)
            ->where('created_at', '>',  Carbon::now()->subHours(1))
            ->first();
        if (empty($rememberToken)) {
            return null;
        }
        return $this->model->where("email", $rememberToken->email)->first();
    }
}
