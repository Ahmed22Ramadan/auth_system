<?php

namespace App\Providers;

use App\Repository\UserRepository;
use App\Services\SocialUserResolver;
use Illuminate\Support\ServiceProvider;
use App\Repository\UserRepositoryInterface;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use App\Services\SocialAccountsService;


class AppServiceProvider extends ServiceProvider
{

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];


    /**
     * Register any application services.
     *
     * @return void
     */
    
    public function register()
    {
        //
        $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
    }
}
