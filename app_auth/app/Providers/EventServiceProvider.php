<?php

namespace App\Providers;

use App\Events\InviteUserEvent;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\SignUpVerificationMailEvent;
use App\Listeners\SignUpVerificationListener;
use App\Events\UserResetPasswordEvent;
use App\Listeners\InviteUserListener;
use App\Listeners\UserResetPasswordListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SignUpVerificationMailEvent::class=>[
            SignUpVerificationListener::class,
        ],
        UserResetPasswordEvent::class=>[
            UserResetPasswordListener::class,
        ],
        InviteUserEvent::class=>[
            InviteUserListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
