<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class UserHasNoPermission implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $user_id;
    public function __construct($user_id = null)
    {
        $this->user_id = $user_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::find($this->user_id);
        if(is_null($user)){
            return true;
        }
        return !($user->can($value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("api.user_already_has_permission");
    }
}
