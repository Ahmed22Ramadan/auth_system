<?php

namespace App\Rules;

use App\Models\Role;
use Illuminate\Contracts\Validation\Rule;

class RoleHasPermission implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $role_id;
    public function __construct($role_id = null)
    {
        $this->role_id = $role_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rolesPermissions = Role::find($this->role_id)->permissions;
        foreach($rolesPermissions as $permission){
            if($permission->slug == $value){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("api.role_doesnt_have_permission");
    }
}
