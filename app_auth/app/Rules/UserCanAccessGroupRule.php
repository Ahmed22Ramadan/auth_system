<?php

namespace App\Rules;

use App\Models\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;

class UserCanAccessGroupRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $groupName;
    public function __construct(string $groupName = null)
    {
        $this->groupName = $groupName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $group = Group::where("name",$this->groupName)->first();
        if(!$group){
            return true;
        }
        return Auth::user()->can("access-group",$group);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("api.not_authorized");
    }
}
