<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Role;

class RoleHasNoPermission implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $role_id;
    public function __construct($role_id = null)
    {
        $this->role_id = $role_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rolesPermissions = Role::find($this->role_id)->permissions;
        foreach($rolesPermissions as $permission){
            if($permission->slug == $value){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("api.role_already_has_permission");
    }
}
