<?php

namespace App\Http\Controllers\api;

use Exception;
use App\Helpers\ApiResponse;
use App\Events\InviteUserEvent;
use App\Http\Requests\GroupRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\EmailNotSentException;
use App\Models\Invite;
use App\Repository\Group\GroupRepositoryInterface;
use App\Repository\User\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class GroupController extends Controller
{
    
    public function __construct(ApiResponse $apiResponse, GroupRepositoryInterface $groupRepository)
    {
        $this->apiResponse = $apiResponse;
        $this->groupRepository = $groupRepository;
    }


    public function all(GroupRequest $groupRequest)
    {
        return $this->groupRepository->all();
    }


    public function Create(GroupRequest $groupRequest)
    {
        return $this->groupRepository->create($groupRequest->validated());
    }

    public function invite(GroupRequest $groupRequest)
    {
        DB::beginTransaction();
        try {
            Invite::create(["email" => $groupRequest->email,"name"=>$groupRequest->name, "user_id" => Auth::user()->id]);
            event(new InviteUserEvent($groupRequest->validated()));
            DB::commit();
            return $this->apiResponse->setSuccess(__("api.invitation_sent_successfully"))->setData()->getJsonResponse();
        } catch (Exception $exception) {
            DB::rollback();
            throw new EmailNotSentException($exception, $this->apiResponse);
        }
    }

    public function accept(GroupRequest $groupRequest, UserRepositoryInterface $userRepository)
    {

        $invite = Invite::where("user_id", $groupRequest->user_id)->first();
        if (!$invite || !Hash::check($invite->email, $groupRequest->hashed_email)) {
            return $this->apiResponse->setError(__("api.invalid_invitation"))->setData()->getJsonResponse();
        }
        $member = $userRepository->create(array_merge($groupRequest->only("name","password"),["email"=>$invite->email]));
        $member->markEmailAsVerified();
        $group = $this->groupRepository->getGroupByName($invite->name);
        $invite->delete();
        $group->members()->attach($member);
        return $this->apiResponse->setSuccess(__("api.member_accepted"))->setData()->getJsonResponse(); bm
    }

    public function delete(GroupRequest $groupRequest){
        $this->groupRepository->getGroupByName($groupRequest->name)->delete();
        return $this->apiResponse->setSuccess(__("api.group_deleted_success"))->setData()->getJsonResponse(); 
    }
}


