<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use App\Helpers\ApiResponse;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Repository\User\UserRepositoryInterface;


class UserController extends Controller
{
    public function __construct(UserRepositoryInterface $userRepository, ApiResponse $apiResponse)
    {
        $this->userRepository = $userRepository;
        $this->apiResponse = $apiResponse;
    }

    /**Start function to get all users*/
    public function all(UserRequest $userRequest)
    {
        return $this->apiResponse->setSuccess(__("api.registerd_users_data"))->setData($this->userRepository->all())->getJsonResponse();
    }
    /**End function to get all users*/

    /**Start function to create user */
    public function create(UserRequest $userRequest){
        $user = $this->userRepository->create($userRequest->validated());
        $user->markEmailAsVerified();
        return $this->apiResponse->setSuccess(__("api.user_created_successfully"))->setData($user)->getJsonResponse(); 
    }
    /**End function to create user */

    /**Start Function to delete User */
    public function delete(UserRequest $userRequest)
    {
        User::find($userRequest->id)->delete();
        return $this->apiResponse->setSuccess(__("api.user_deleted_successfully",["name"=>$userRequest->user()->roles->first()->name]))->setData()->getJsonResponse();
    }
    /**#End Function to delete User */

    /**Start a Function to update user*/
    public function update(UserRequest $userRequest)
    {
        $user = User::where("id", $userRequest->id);
        $user->update([
            "name" => $userRequest->name,
            "email" => $userRequest->email,
        ]);
        return $this->apiResponse->setSuccess(__("api.user_updated_successfully",["name"=>$userRequest->user()->roles->first()->name]))->setData($user->first())->getJsonResponse();
    }
    /**#End a Function to update user*/
}
