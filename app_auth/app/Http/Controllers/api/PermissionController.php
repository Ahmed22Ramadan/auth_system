<?php

namespace App\Http\Controllers\api;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionsRequest;
use App\Repository\Permission\PermissionRepositoryInterface;
use App\Repository\User\UserRepository;

class PermissionController extends Controller
{
    public function __construct(ApiResponse $apiResponse, PermissionRepositoryInterface $permissionRepository)
    {
        $this->apiResponse = $apiResponse;
        $this->permissionRepository = $permissionRepository;
    }


    /**Start a function to get all the permissions */
    public function all()
    {
        return $this->apiResponse->setSuccess(__("api.all_permissions"))
        ->setData($this->permissionRepository->all())
        ->getJsonResponse();
    }
    /**#End a function to get all the permissions */

    /**Start a function to add the permission */
    public function create(PermissionsRequest $permissionsRequest)
    {
        return $this->apiResponse->setSuccess(__("api.permission_created_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData($this->permissionRepository->create($permissionsRequest->validated()))
            ->getJsonResponse();
    }
    /**#End a function to add the permission */

    
    /**Start a function to add the permission */
    public function delete(PermissionsRequest $permissionsRequest)
    {
        $this->permissionRepository->delete($permissionsRequest->id);
        return $this->apiResponse->setSuccess(__("api.permission_deleted_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData($this->permissionRepository->all())
            ->getJsonResponse();
    }
    /**#End a function to add the permission */

    /**start a function to give the permission to a user */
    public function assignPermission(PermissionsRequest $permissionsRequest, UserRepository $userRepository)
    {
        $user = $userRepository->find($permissionsRequest->user_id);
        $user  = $user->givePermissionTo($permissionsRequest->slug);
        return $this->apiResponse->setSuccess(__("api.permission_assigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["user" => $user, "permissions" => $user->permissions])
            ->getJsonResponse();
    }
    /**End a function to give the permission to a user */

    /**Start a function to remove the permissions from a user  */
    public function unassignPermission(PermissionsRequest $permissionsRequest, UserRepository $userRepository)
    {
        $user = $userRepository->find($permissionsRequest->user_id);
        $user = $user->withdrawPermissionTo($permissionsRequest->slug);
        return $this->apiResponse->setSuccess(__("api.permission_unassigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["user" => $user, "permissions" => $user->permissions])
            ->getJsonResponse();
    }
    /**End a function to remove the permission from a user  */
}
