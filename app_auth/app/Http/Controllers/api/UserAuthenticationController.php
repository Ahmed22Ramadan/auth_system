<?php

namespace App\Http\Controllers\api;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Helpers\Constants;
use App\Helpers\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Services\SocialUserResolver;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Events\UserResetPasswordEvent;
use App\Events\SignUpVerificationMailEvent;
use App\Exceptions\EmailNotSentException;
use App\Exceptions\HandleApiErrorsException;
use Laravel\Passport\Client as PassportClient;
use App\Http\Requests\UserAuthenticationRequest;
use App\Repository\User\UserRepositoryInterface;

class UserAuthenticationController extends Controller
{
    protected $apiResponse;
    protected $connection;
    protected $userRepository;
    protected $client;
    public function __construct(ApiResponse $apiResponse, UserRepositoryInterface $userRepository, Client $client)
    {
        $this->apiResponse = $apiResponse;
        $this->userRepository = $userRepository;
        $this->client = $client;
    }


    // Start function to sign up Users
    public function register(UserAuthenticationRequest $userAuthenticationRequest)
    {
        DB::beginTransaction();
        try {
            $user = $this->userRepository->create($userAuthenticationRequest->validated());
            event(new SignUpVerificationMailEvent($userAuthenticationRequest->validated()));
            DB::commit();
            return $this->apiResponse
                ->setSuccess(__("api.not_verified_yet"))->setData($user)->getJsonResponse();
        } catch (Exception $exception) {
            DB::rollback();
            throw new EmailNotSentException($exception, $this->apiResponse);
        }
    }
    // #End function to sign up Users 

    // Start function to authenticate the user 
    public function login(UserAuthenticationRequest $userAuthenticationRequest)
    {
        $user = $this->userRepository->getUserByEmail($userAuthenticationRequest->email);
        if (!$user->hasVerifiedEmail()) {
            return $this->apiResponse
                ->setError(__("api.not_verified"))->setData()->getJsonResponse();
        }
        if (!Auth::attempt($userAuthenticationRequest->only("email", "password"))) {
            return $this->apiResponse
                ->setError(__("api.not_authorized"))->setData()->getJsonResponse();
        }
        $passportClient = PassportClient::where("password_client", 1)->first();
        $formParams = [
            'grant_type' => 'password',
            'client_id' => $passportClient->id,
            'client_secret' => $passportClient->secret,
            'username' => $userAuthenticationRequest->email,
            'password' => $userAuthenticationRequest->password,
            'scope' => '*'
        ];

        try {
            $passportResponse = $this->client->request('POST', Constants::MY_HOST . "/oauth/token", ['form_params' => $formParams]);
            $passportResponse = json_decode((string) $passportResponse->getBody(), true);
            $finalResponse = array_merge($passportResponse, ["user" => Auth::user()]);

            return $this->apiResponse
                ->setSuccess(__("api.success_login"))
                ->setData($finalResponse)
                ->getJsonResponse();
        } catch (Exception $exception) {
            throw new HandleApiErrorsException($exception, $this->apiResponse);
        };
    }
    //#End functtion to authenticate user 

    //Start Function to Verify User
    public function verifyUser(UserAuthenticationRequest $userAthenticationRequest)
    {
        $user = $this->userRepository->findUnexpiredUser($userAthenticationRequest->id);

        if (empty($user) || !(Hash::check($user->email, $userAthenticationRequest->hashed_email))) {
            return $this->apiResponse->setData()->setError(__("api.verification_error"))->getJsonResponse();
        }
        $user->markEmailAsVerified();
        return $this->apiResponse->setData($user)->setSuccess(__("api.success_verification"))->getJsonResponse();
    }
    //#End funvtion to  Verify user

    //Start Get Authorized User
    public function user(UserAuthenticationRequest $userAuthenticationRequest)
    {
        return $this->apiResponse->setSuccess(__("api.user_data"))->setData(request()->user())->getJsonResponse();
    }
    //#End Get Authorized User

    //Start Revoking The Personal Token
    public function logout(UserAuthenticationRequest $userAuthenticationRequest)
    {
        request()->user()->token()->revoke();
        return $this->apiResponse->setSuccess(__("api.logged_out_successfully"))->setData()->getJsonResponse();
    }
    //#End Revoking The Personal Token


    //Start public function to Reset The Password
    public function resetPassword(UserAuthenticationRequest $userAuthenticationRequest)
    {
        $user = $this->userRepository->getUserByEmail($userAuthenticationRequest->email);
        if (!$user->hasVerifiedEmail()) {
            return $this->apiResponse->setError(__("api.not_verified"))->setData()->getJsonResponse();
        }
        $token = app('auth.password.broker')->createToken($user);
        DB::table('password_resets')->insert([
            'email' => $userAuthenticationRequest->email,
            'token' => $token,
            'created_at' => Carbon::now(),
        ]);
        $user->token = $token;
        event(new UserResetPasswordEvent($user));
        unset($user->token);
        return $this->apiResponse->setSuccess(__("api.email_sent_successfully"))->setData($user)->getJsonResponse();
    }
    //#End public function to Reset The Password

    //Start function to check if the user is a member
    public function changePassword(UserAuthenticationRequest $userAuthenticationRequest)
    {
        $user =  $this->userRepository->getUserByToken($userAuthenticationRequest->token);
        if ($user == null) {
            return $this->apiResponse->setError(__("api.not_registered"))->setData()->getJsonResponse();
        }
        $user->update([
            "password" => $userAuthenticationRequest->password,
        ]);
        DB::table('password_resets')->where('token', $user->email)->delete();
        return $this->apiResponse->setSuccess(__("api.passowed_changed_successfully"))->setData($user)->getJsonResponse();
    }
    //#End function to check if user is a member

    public function refreshToken(Request $request)
    {
        if (is_null($request->header('refresh_token'))) {
            return $this->apiResponse->setError(__("api.not_authorized"))->setData()->getJsonResponse();
        }
        $refresh_token = $request->header('refresh_token');
        $passportClient = PassportClient::where("password_client", 1)->first();
        $formParams = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id' => $passportClient->id,
            'client_secret' => $passportClient->secret,
            'scope' => ''
        ];

        try {
            $passportResponse = $this->client->request('POST', Constants::MY_HOST . "/oauth/token", ['form_params' => $formParams]);
            $passportResponse = json_decode((string) $passportResponse->getBody(), true);
            return $this->apiResponse->setSuccess(__("api.logged_in_successfully"))->setData($passportResponse)->getJsonResponse();
        } catch (Exception $exception) {
            throw new HandleApiErrorsException($exception, $this->apiResponse);
        };
    }

    public function socialLogin(UserAuthenticationRequest $userRequest, SocialUserResolver $socialUserResolver)
    {
        try {
            $user = $socialUserResolver->resolveUserByProviderCredentials($userRequest->provider, $userRequest->access_token);
            $passportClient = PassportClient::where("password_client", 1)->first();
            $formParams = [
                'grant_type' => 'social',
                'client_id' => $passportClient->id,
                'client_secret' => $passportClient->secret,
                'provider' => $userRequest->provider,
                'access_token' => $userRequest->access_token,
            ];
            $response = $this->client->post(Constants::MY_HOST . '/oauth/token', ['form_params' => $formParams]);
            $response =  ['user' => $user, "token_data" => json_decode($response->getBody()->getContents(), true)];
            return $this->apiResponse
                ->setSuccess(__("api.logged_in_successfully"))
                ->setData($response)
                ->getJsonResponse();
        } catch (Exception $exception) {
            throw new HandleApiErrorsException($exception, $this->apiResponse);
        }
    }
}
