<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
/**
 * @OA\Info(
 *    title="my authentication system",
 *    version="1.0.0",
 * )
 */

/**
 * @OA\Post(
 * path="/api/login",
 * summary="Sign in",
 * description="Login by email, password",
 * operationId="authLogin",
 * tags={"auth"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="user", type="string", example="Success message"),
 *    )
 * )
 *@OA\post(
 * path="/api/users",
 * summary="Retrieve Users Data",
 * description="Get Users Information",
 * operationId="usersGet",
 * tags={"Users"},
 * security={ {"bearer": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success message")
 *        )
 *     ),
 * @OA\Response(
 *    response=401,
 *    description="User should be authorized to get users information",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * ) 
 * @OA\post(
 * path="/api/logout",
 * summary="forget the session of this user",
 * description="loging user out",
 * operationId="userLogout",
 * tags={"Users"},
 * security={ {"bearer": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="success message")
 *        )
 *     ),
 * ) 
 */
 

class ApiController extends Controller
{
    //
}
