<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Helpers\ApiResponse;
use App\Http\Requests\RolesRequest;
use App\Repository\Role\RoleRepositoryInterface;
use App\Repository\User\UserRepository;

class RoleController extends Controller
{

    public function __construct(ApiResponse $apiResponse, RoleRepositoryInterface $roleRepository)
    {
        $this->apiResponse = $apiResponse;
        $this->roleRepository = $roleRepository;
    }
    /**Start a functoin to get all the Roles */
    public function all()
    {
        return $this->apiResponse->setSuccess(__("api.all_roles"))
            ->setData($this->roleRepository->all())
            ->getJsonResponse();
    }
    /**#End a functoin to get all the Roles */

    /**Start a function to create a user Role */
    public function create(RolesRequest $rolesRequest)
    {
        return $this->apiResponse->setSuccess(__("api.role_created_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData($this->roleRepository->create($rolesRequest->validated()))
            ->getJsonResponse();
    }
    /**#End a function to create a user Role */


    /**Start a function to delete a  role */
    public function delete(RolesRequest $rolesRequest)
    {
        $this->roleRepository->delete($rolesRequest->id);
        return $this->apiResponse->setSuccess(__("api.role_deleted_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData($this->roleRepository->all())
            ->getJsonResponse();
    }
    /**End function to delete a role */

    /**start a function to give the role to a user */
    public function assignRole(rolesRequest $rolesRequest, UserRepository $userRepository)
    {
        $user = $userRepository->find($rolesRequest->user_id);
        $user  = $user->giveRoleTo($rolesRequest->slug);
        return $this->apiResponse->setSuccess(__("api.role_assigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["user" => $user, "roles" => $user->roles])
            ->getJsonResponse();
    }
    /**End a function to give the role to a user */

    /**start a function to give the role to a user */
    public function unassignRole(rolesRequest $rolesRequest, UserRepository $userRepository)
    {
        $user = $userRepository->find($rolesRequest->user_id);
        $user = $user->withdrawRoleTo($rolesRequest->slug);
        return $this->apiResponse->setSuccess(__("api.role_unassigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["user" => $user, "roles" => $user->roles])
            ->getJsonResponse();
    }
    /**End a function to give the role to a user */
    
    /**Start a function to add permission to role */
    public function assignPermissionToRole(RolesRequest $rolesRequest){
        $role = $this->roleRepository->find($rolesRequest->id);
        $role  = $role->givePermissionTo($rolesRequest->slug);
        return $this->apiResponse->setSuccess(__("api.permission_assigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["role" => $role, "permissions" => $role->permissions])
            ->getJsonResponse();
    }
    /**#End a function to add permission to role */

    /**start a function to give the role to a user */
    public function unassignPermissionFromRole(rolesRequest $rolesRequest, UserRepository $userRepository)
    {
        $role =  $this->roleRepository->find($rolesRequest->id);
        $role = $role->withdrawPermissionTo($rolesRequest->slug);
        return $this->apiResponse->setSuccess(__("api.permission_unassigned_successfully",["name"=>$rolesRequest->user()->roles->first()->name]))
            ->setData(["role" => $role, "roles" => $role->permissions])
            ->getJsonResponse();
    }
    /**End a function to give the role to a user */
}
