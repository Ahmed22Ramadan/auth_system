<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CheckVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::user()->hasVerifiedEmail()){
            return response()->json(["message"=>__("api.not_verified"),"data"=>null,"status"=>false],415,[],JSON_NUMERIC_CHECK);
        }
        return $next($request);
    }
}
