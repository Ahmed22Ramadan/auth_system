<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        if (is_null($request->user())) {
            return response()->json(["message" => __("api.not_authorized"), "data" => null, "status" => false], 404, [], JSON_NUMERIC_CHECK);
        }
        if (!$request->user()->hasRole($role)) {
            return response()->json(["message" => __("api.not_authorized"), "data" => null, "status" => false], 404, [], JSON_NUMERIC_CHECK);
        }
        if ($permission !== null && !$request->user()->can($permission)) {
            return response()->json(["message" => __("api.not_authorized"),"data" => null,"status" => false], 404, [], JSON_NUMERIC_CHECK);
        }
        return $next($request);
    }
}
