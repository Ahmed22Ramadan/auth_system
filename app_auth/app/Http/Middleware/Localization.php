<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;

class Localization
{

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $language = $request->header("localization");
        if(!$language){
            $language = $this->app->config->get("app.locale");
        }
        if(!array_key_exists($language,$this->app->config->get("app.app_languages"))){
            return response()->json(["message"=>__("api.not_supported_language"),"data"=>null,"status"=>false],403,[],JSON_NUMERIC_CHECK);
        }
        $this->app->setLocale($language);

        $response = $next($request);

        $response->headers->set("localization",$language);
    
        return $response;
    }
}
