<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;


class UserAuthenticationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (($this->path() == "api/user" and  !Auth::user()) || ($this->path() == "api/logout" and !Auth::user())) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->path() == "api/login") {
            return [
                "email" => "required|email|min:8|max:191|exists:users,email",
                "password" => [
                    'required',
                    'min:6',
                    "max:191",
                    'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
                ],
            ];
        }
        if ($this->path() == "api/register") {
            return [
                "email" => "required|email|unique:users,email|min:8|max:191",
                "password" => [
                    'required',
                    'min:6',
                    "max:191",
                    'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                    'confirmed'
                ],
                "name" => "required|min:4|max:191",

            ];
        }
        if ($this->path() == "api/verify/user") {
            return [
                "id" => "required|numeric|min:1|exists:users,id",
                "hashed_email" => "required|string|min:10"
            ];
        }
        if ($this->path() == "api/reset/password") {
            return [
                "email" => "required|email|min:8|max:191|exists:users,email",
            ];
        }
        if ($this->path() == "api/change/password") {
            return [
                "token" => "required|min:10|max:191|exists:password_resets,token",
                "password" => [
                    'required',
                    'min:6',
                    "max:191",
                    'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                    'confirmed'
                ],
            ];
        }
        if ($this->path() == "api/social/login") {
            return [
                "access_token" => "required|string",
                "provider" => "required|string",
            ];
        }
        if ($this->path() == "api/user") {
            return [];
        }
        if ($this->path() == "api/logout") {
            return [];
        }

    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => $validator->errors()->first(),
                    'data' => null
                ],
                400
            )
        );
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => "Error: you are not authorized or do not have the permission",
                    'data' => null
                ],
                400
            )
        );
    }


    public function messages()
    {
        return [
            "email.required" => __('api.email_required'),
            "email.unique" =>  __("api.user_already_exists"),
            "email.email" => __("api.not_valid_email"),
            "email.min" => __("api.email_min"),
            "email.max" => __("api.email_max"),
            "email.exists" => __("api.user_not_exists"),
            "id.exists" =>  __("api.user_not_exists"),
            "password.required" => __("api.password_required"),
            "password.confirmed" => __("api.password_confirmed"),
            "password.min" => __("api.password_min"),
            "password.max" => __("api.password_max"),
            "password.regex" => __("api.password_regex"),
            "name.required" => __("api.name_required"),
            "name.min" =>  __("api.name_min"),
            "name.max" =>  __("api.name_max"),
            "token.required" => __("api.token_required"),
            "token.exists" => __("api.token_exists"),
        ];
    }
}
