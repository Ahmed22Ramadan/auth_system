<?php

namespace App\Http\Requests;

use App\Rules\UserHasPermission;
use App\Rules\UserHasNoPermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!Auth::user() || !(Auth::user()->hasRole("super-admin"))){
            return false;

        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->path() == "api/create/permission") {
            return [
                "slug" => "required|string|min:2|max:191|unique:permissions,slug|regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/",
                "name" => "required|string|min:2|unique:permissions,name|max:191",
            ];
        }
        if ($this->path() == "api/delete/permission") {
            return [
                "id" => "required|integer|min:1|exists:permissions,id",
            ];
        }
        if ($this->path() == "api/assign/permission") {
            return [
                "slug" => ["required","string","exists:permissions,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new UserHasNoPermission($this->id)],
                "user_id" => "required|integer|min:1|exists:users,id",
            ];
        }
        if ($this->path() == "api/unassign/permission") {
            return [
                "slug" => ["required","string","exists:permissions,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new UserHasPermission($this->id)],
                "user_id" => "required|integer|min:1|exists:users,id",
            ];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => $validator->errors()->first(),
                    'data' => null
                ],
                400
            )
        );
    }
    protected function failedAuthorization()
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => __("api.not_authorized"),
                    'data' => null
                ],
                400
            )
        );
    }


    public function messages()
    {
        return [
            "name.required" => __('api.permission_name_required'),
            "name.min" => __("api.permission_name_min"),
            "name.max" => __("api.permission_name_max"),
            "name.unique" => __("api.permission_already_exists"),
            "slug.required" =>  __('api.permission_slug_required'),
            "slug.min" =>  __("api.permission_slug_min"),
            "slug.max" => __("api.permission_slug_max"),
            "slug.unique" => __("api.permission_already_exists"),
            "slug.regex" => __("api.slug_regex"),
            "slug.exists" => __("api.permission_not_exists"),
            "id.required" =>  __('api.permission_id_required'),
            "id.integer" => __("api.permission_id_not_integer"),
            "id.exists" => __("api.permission_not_exists"),
            "id.min" => __("api.permission_id_min"),
            "user_id.required" =>  __('api.user_id_required'),
            "user_id.integer" => __("api.user_id_not_integer"),
            "user_id.exists" => __("api.user_not_exists"),
            "user_id.min" => __("api.user_id_min"),

        ];
    }
}
