<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!Auth::user() || !Auth::user()->hasRole('super-admin')){
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->path() == "api/users") {
            return [];
        }
        if ($this->path() == "api/delete/user") {
            return [
                "id" => "required|exists:users,id|integer|min:1",
            ];
        }


        if ($this->path() == "api/update/user") {
            return [
                "id" => "required|exists:users,id|integer|min:1",
                "name" => "required|min:4|max:191",
                "email" => "required|email|min:8|max:191|unique:users,email," . $this->id,
            ];
        }
        if ($this->path() == "api/create/user") {
            return [
                "email" => "required|email|unique:users,email|min:8|max:191",
                "password" => [
                    'required',
                    'min:6',
                    "max:191",
                    'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                    'confirmed'
                ],
                "name" => "required|min:4|max:191",
            ];
        }
        if ($this->path() == "api/users") {
            return [
            
            ];
        }
    }
    public function messages()
    {
        return [
            "user_id.required" =>  __('api.user_id_required'),
            "user_id.integer" => __("api.user_id_not_integer"),
            "user_id.exists" => __("api.user_not_exists"),
            "user_id.min" => ("api.user_id_min"),
            "id.exists" =>  __("api.user_not_exists"),
            "email.required" => __('api.email_required'),
            "email.unique" =>  __("api.user_already_exists"),
            "email.email" => __("api.not_valid_email"),
            "email.min" => __("api.email_min"),
            "email.max" => __("api.email_max"),
            "email.exists" => __("api.user_not_exists"),
            "name.required" => __("api.name_required"),
            "name.min" =>  __("api.name_min"),
            "name.max" =>  __("api.name_max"),
            "password.required" => __("api.password_required"),
            "password.confirmed" => __("api.password_confirmed"),
            "password.min" => __("api.password_min"),
            "password.max" => __("api.password_max"),
            "password.regex" => __("api.password_regex")

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => $validator->errors()->first(),
                    'data' => null
                ],
                400
            )
        );
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => "Sorry, you are not authorized or do not have the permission",
                    'data' => null
                ],
                403
            )
        );
    }
}
