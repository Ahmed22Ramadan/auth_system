<?php

namespace App\Http\Requests;

use App\Rules\UserCanAccessGroupRule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;



class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!Auth::user() || ($this->path() == "api/groups" and !Auth::user()->hasRole('super-admin'))) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->path() == "api/groups") {
            return [
                //
            ];
        }

        if ($this->path() == "api/create/group") {
            return [
                "name" => "string|required|unique:groups,name|min:4|max:191",
                "user_id" => "required|integer|exists:users,id|min:1",
            ];
        }
        if ($this->path() == "api/invite/user") {
            return [
                "email" => ["required","unique:users,email","email",new UserCanAccessGroupRule($this->name)],
                "name" => "required|string|exists:groups,name|unique:invites,name"
            ];
        }
        if ($this->path() == "api/accept/user") {
            return [
                "user_id" => "required|integer|exists:users,id|min:1",
                "hashed_email" => "required|min:10",
                "name" => "string|min:4|max:191|unique:users,name",
                "password" => [
                    'required',
                    'min:6',
                    "max:191",
                    'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                    'confirmed'
                ]
            ];
        }
        if ($this->path() == "api/delete/group") {
            return [
                "name" => ["required","string","exists:groups,name",new UserCanAccessGroupRule($this->name)]
            ];
        }
    }



    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => $validator->errors()->first(),
                    'data' => null
                ],
                400
            )
        );
    }


    protected function failedAuthorization()
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => __("api.not_authorized"),
                    'data' => null
                ],
                400
            )
        );
    }


    public function messages()
    {
        if ($this->path() == "api/accept/user") {
            return [
                "name.required" => __('api.user_name_required'),
                "name.min" => __("api.user_name_min"),
                "name.max" => __("api.user_name_max"),
                "password.required" => __("api.password_required"),
                "password.confirmed" => __("api.password_confirmed"),
                "password.min" => __("api.password_min"),
                "password.max" => __("api.password_max"),
                "password.regex" => __("api.password_regex"),
            ];
        }
        
        return [
            "name.required" => __('api.group_name_required'),
            "name.min" => __("api.group_name_min"),
            "name.max" => __("api.group_name_max"),
            "name.unique" => __("api.name_already_exists"),
            "name.exisits" => __("api.group_not_exists"),
            "user_id.required" =>  __('api.user_id_required'),
            "user_id.integer" => __("api.user_id_not_integer"),
            "user_id.exists" => __("api.user_not_exists"),
            "user_id.min" => __("api.user_id_min"),
            "group_id.required" =>  __('api.group_id_required'),
            "group_id.integer" => __("api.group_id_not_integer"),
            "group_id.exists" => __("api.group_not_exists"),
            "group_id.min" => __("api.group_id_min"),
            "email.required" => __('api.email_required'),
            "email.email" => __("api.not_valid_email"),
            "email.unique" => __("api.user_already_exists"),
        ];
    }
}
