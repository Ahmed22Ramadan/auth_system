<?php

namespace App\Http\Requests;

use App\Rules\UserHasRole;
use App\Rules\UserHasNoRole;
use App\Rules\RoleHasPermission;
use App\Rules\RoleHasNoPermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class RolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!Auth::user() || !(Auth::user()->hasRole("super-admin"))){
            return false;

        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->path() == "api/create/role") {
            return [
                "slug" => "required|string|unique:roles,slug|min:2|max:191|regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/",
                "name" => "required|string|unique:roles,name|min:2|max:191",
            ];
        }
        if ($this->path() == "api/delete/role") {
            return [
                "id" => "required|integer|min:1|exists:roles,id",
            ];
        }
        if ($this->path() == "api/assign/role") {
            return [
                "slug" => ["required","string","exists:roles,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new UserHasNoRole($this->id)],
                "user_id" => "required|integer|min:1|exists:users,id",
            ];
        }
        if ($this->path() == "api/unassign/role") {
            return [
                "slug" => ["required","string","exists:roles,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new UserHasRole($this->id)],
                "user_id" => "required|integer|min:1|exists:users,id",
            ];
        }
        if ($this->path() == "api/assign/role/permission") {
            return [
                "slug" => ["required","string","exists:permissions,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new RoleHasNoPermission($this->id)],
                "id" => "required|integer|min:1|exists:roles,id",
            ];
        }
        if ($this->path() == "api/unassign/role/permission") {
            return [
                "slug" => ["required","string","exists:permissions,slug","min:2","max:191","regex:/^([a-zA-Z]*-[a-zA-Z]*)+$/", new RoleHasPermission($this->id)],
                "id" => "required|integer|min:1|exists:roles,id",
            ];
        }
        
    }


    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => $validator->errors()->first(),
                    'data' => null
                ],
                400
            )
        );
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'status' => false,
                    'message' => "you are not authorized or do not have the permission",
                    'data' => null
                ],
                400
            )
        );
    }

    public function messages()
    {
        return [

            "name.required" => __('api.role_name_required'),
            "name.unique" => __("api.role_already_exists"),
            "name.min" => __("api.role_name_min"),
            "name.max" => __("api.role_name_max"),
            "slug.required" => __('api.role_slug_required'),
            "slug.min" =>  __("api.role_slug_min"),
            "slug.max" => __("api.role_slug_max"),
            "slug.regex" => __("api.slug_regex"),
            "slug.unique" => __("api.role_already_exists"),
            "slug.exists" =>  __("api.role_not_exists"),
            "user_id.required" =>  __('api.user_id_required'),
            "user_id.integer" => __("api.user_id_not_integer"),
            "user_id.exists" => __("api.user_not_exists"),
            "user_id.min" => ("api.user_id_min"),
            "id.required" => __('api.role_id_required'),
            "id.integer" => __("api.role_id_not_integer"),
            "id.exists" => __("api.role_not_exists"),
            "id.min" => __("api.role_id_min"),
            
        ];
    }
}
