<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SignUpVerificationEmail;
use App\Models\User;
use App\Notifications\VerifiyEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SignUpVerificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->userData["hashed_email"] = Hash::make($event->userData["email"]);
        $user = User::Email($event->userData["email"])->first();
        $event->userData["id"] = $user->id;
        $user->notify(new VerifiyEmail($event->userData));
    }
}
