<?php

namespace App\Listeners;

use App\Models\Group;
use App\Models\User;
use App\Notifications\InviteUserNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification
;use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;


class InviteUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->data["hashed_email"] = Hash::make($event->data["email"]);
        Notification::route('mail', $event->data["email"])
            ->notify(new InviteUserNotification($event->data));
    }
}
