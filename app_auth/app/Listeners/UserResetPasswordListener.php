<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Mail\UserResetPasswordMail;
use App\Notifications\ResetPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserResetPasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->user->notify(new ResetPassword($event->user));
    }
}
