<?php


return [
    /**Start UserAuthenticationController localization */
    "success_login" => "You logged in successfully, welcome to our application.",
    "not_verified_yet" => "User Created But Not Verified Yet Please Check Your Email for Verification.",
    "send_email_error" => "Error while sending email or adding user, please check your internet connection.",
    "not_verified" => "you did not verify this user through our sent verification email, please verifiy your account.",
    "not_authorized" => "you are not authorized or do not have the permission.",
    "verification_error" => "Error:'No Suche User Registered With Us Or The Verification email is expired.'",
    "success_verification" => "User Has been Verified Successfully.",
    "server_error" => "Server Error, Please Check Your Internet Connection.",
    "logged_in_successfully" => "you logged in successfully",
    "email_sent_successfully" => "Email has been sent successfully",
    "user_data" => "here is the logged in user data",
    "not_registered" => "There Is No Such User Registered With Us",
    "passowed_changed_successfully" => "Password Has Been Changed Successfully",
    "logged_out_successfully" => "you logged out successfully",
    /**End UserAuthenticationController localization */

    /**Start UserController localization */
    "registerd_users_data" => "here is our registered users.",
    "user_created_successfully" => "user has been created successfully",
    "user_deleted_successfully" => "Hi :name, user has been deleted successfully.",
    "user_updated_successfully" => "Hi :name, user has been updated successfully.",
    /**End UserController localization */

    /**Start RoleController localization */
    "all_roles" => "here are the roles we can give.",
    "role_created_successfully" => "Hi :name, role has been created successfully.",
    "role_deleted_successfully" => "Hi :name, role has been deleted successfully.",
    "role_assigned_successfully" => "Hi :name, role has been given successfully.",
    "role_unassigned_successfully" => "Hi :name, role has been unassigned successfully.",
    "permission_assigned_successfully" => "Hi :name, permission has been given successfully.",
    "permission_unassigned_successfully" => "Hi :name, permission has been unassigned successfully.",
    /**End RoleController localization */

    /**Start PermissionController localization */
    "all_permissions" => "here are the permissions we can give.",
    "permission_created_successfully" => "Hi :name, permission has been created successfully.",
    "permission_deleted_successfully" => "Hi :name, permission has been deleted successfully.",
    "permission_assigned_successfully" => "Hi :name, permission has been given successfully.",
    "permission_unassigned_successfully" => "Hi :name, permission has been unassigned successfully.",
    /**End PermissionController localization */

    /**Start PermissionRequest localization */
    "permission_name_required" => "please enter your permission name first",
    "permission_slug_required" => "please enter your permission slug first",
    "permission_id_required" => "please enter your permission id first",
    "permission_name_min" => "oops!! the permission name field must be at least 3 characters",
    "permission_id_min" => "oops!! the permission id field must be at least 1",
    "permission_slug_min" => "oops!! the permission slug field must be at least 3 characters",
    "permission_name_max" => "oops!! the permission name field must be at most 191 characters",
    "permission_slug_max" => "oops!! the permission slug field must be at most 191 characters",
    "user_id_required" => "please enter your user id first",
    "user_id_min" => "oops!! the user id field must be at least 1",
    "permission_already_exists" => "This permission already exists",
    "permission_slug_regex" => "oops!! the permission slug field must on hyphens and no numbers",
    "permission_not_exists" => "oops!! this permission does not exist.",
    "permission_id_not_integer" => "permission id must be integer",
    "user_id_not_integer" => "user id must be integer",
    "user_not_exists" => "oops!! this user does not exist.",
    /**End PermissionRequest localization */

    /**Start RoleRequest localization */
    "role_name_required" => "please enter your role name first",
    "role_slug_required" => "please enter your role slug first",
    "role_id_required" => "please enter your role id first",
    "role_name_min" => "oops!! the role name field must be at least 3 characters",
    "role_id_min" => "oops!! the role id field must be at least 1",
    "role_slug_min" => "oops!! the role slug field must be at least 3 characters",
    "role_name_max" => "oops!! the role name field must be at most 191 characters",
    "role_slug_max" => "oops!! the role slug field must be at most 191 characters",
    "user_id_required" => "please enter your user id first",
    "user_id_min" => "oops!! the user id field must be at least 1",
    "role_already_exists" => "This role already exists",
    "slug_regex" => "oops!! the slug field must on hyphens and no numbers",
    "role_not_exists" => "oops!! this role does not exist.",
    "role_id_not_integer" => "role id must be integer",
    "user_id_not_integer" => "user id must be integer",
    "user_not_exists" => "oops!! this user does not exist.",
    /**End RoleRequest localization */

    /**Start UserRequest localization */
    "user_already_exists" => "oops!! this user already exist.",
    "not_valid_email" => "oops!! That is not a valid email.",
    "email_min" => "oops!! the email field must be at least 8 characters.",
    "email_max" => "oops!! the email field must be at most 191 characters.",
    "email_required" => "Please, enter the email first.",
    "name_required" => "Please Enter Your Name.",
    "name_min" => "oops!! name must be at least 4 characters.",
    "name_max" => "oops!! the name field must be at most 191 characters.",
    "password_required" => "please enter your password first.",
    "password_confirmed" => "oops!! password must be confirmed.",
    "password_min" => "oops!! the password field must be at least 6 characters.",
    "password_max" => "oops!! the password field must be at most 191 characters .",
    "password_regex" => "oops!! the passowrd must  contain numbers, capital letters, small letters and special characters",
    "token_exists" => "oops!! you have to go and reset your password again",
    "token_required" => "oops!! token is required",
    
    /**End UserRequest localization */
    
    /**Start GroupRequest Localization */
    "group_name_required" => "please enter your Group name first.",
    "group_name_min" => "oops!! the Group name field must be at least 3 characters.",
    "group_name_max" => "oops!! the Group name field must be at most 191 characters.",
    "group_already_exists" => "This Group already exists.",
    "group_id_required" => "please enter your group id first.",
    "group_id_min" => "oops!! the group id field must be at least 1.",
    "group_not_exists" => "oops!! this group does not exist.",
    "group_id_not_integer" => "group id must be integer.",
    "member_id_required" => "please enter your member id first.",
    "member_id_min" => "oops!! the member id field must be at least 1.",
    "member_not_exists" => "oops!! this member does not exist.",
    "member_id_not_integer" => "member id must be integer.",
    "invitation_sent_successfully" => "Invitation Has Been Sent Successfully.",
    "invalid_invitation" => "This invitation is invalid, please check your mail box.",
    "member_accepted" => "Member has been accepted successfully.",
    "group_deleted_success" => "Group has been deleted successfully.",
    
    /**End GroupRequest Localization */

    /**Start others */
    "not_supported_language" => "sorry, This languages is not supported in our application.",
    "role_doesnt_have_permission" => 'This role does not have this permission.',
    "role_already_has_permission" => "This role already have this permission.",
    "user_doesnt_have_permission" => 'User already does not have this permission.',
    "user_doesnt_have_role" => 'This user already does not have this role.',
    "user_already_has_permission" => 'User already has this permission.',
    "user_already_has_role" =>  'This User already have This role.'
    /**End others */



];
