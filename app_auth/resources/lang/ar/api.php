<?php


return [
    /**Start UserAuthenticationController localization */
    "success_login" => ".لقد تم تسجيل الدخول بنجاح",
    "not_verified_yet" => ".لقد تم إنشاء العضوية, لكن رجاء قم بفحص البريد الخاص بك لتفعيل الحساب",
    "send_email_error" => ".حدث خطأ اثناء ارسال الايميل, من فضلك تأكد من الاتصال بالانترنت",
    "not_verified" => "!!!هذا الحساب غير مفعل",
    "not_authorized" => "عذرا, ليس لديك الصلاحية لذلك",
    "verification_error" => ".هذا العميل ليس لديه حساب لدينا, قم بإنشاء حساب اولا",
    "success_verification" => ".لقد تم تفعيل العضوية بنجاح",
    "server_error" => ".من فضلك تاكد من الاتصال بالانترنت",
    "logged_in_successfully" => ".تم تسجيل الدخول بنجاح",
    "email_sent_successfully" => ".تم ارسال البريد بنجاح",
    "user_data" => ".بيانات العميل الحالي",
    "not_registered" => ".هذا العميل لم يقم بالتسجيل لدينا",
    "passowed_changed_successfully" => ".تم تعديل الرقم السري بنجاح",
    "logged_out_successfully" => "تم تسجيل الخروج بنجاح",
    /**End UserAuthenticationController localization */

    /**Start UserController localization */
    "registerd_users_data" => ".بيانات العملاء المسجلين لدينا",
    "user_created_successfully" => ".تم انشاء العميل بنجاح",
    "user_deleted_successfully" => ".تم حذف العميل بنجاح :name اهلا",
    "user_updated_successfully" => ".تم تعديل العميل بنجاح :name اهلا",
    /**End UserController localization */

    /**Start RoleController localization */
    "all_roles" => ".هذه هي الأدوار التي يمكن اعظائها للعميل",
    "role_created_successfully" => ".تم انشاء الدور بنجاح :name اهلا",
    "role_deleted_successfully" => ".تم حذف  الدور بنجاح :name اهلا",
    "role_assigned_successfully" => ".تم منح الدور بنجاح :name اهلا",
    "role_unassigned_successfully" => ".تم حذف الدور من العميل بنجاح :name اهلا",
    "permission_assigned_successfully" => ".تم منح التصريح بنجاح :name اهلا",
    "permission_unassigned_successfully" => ".تم حذف التصريح من العميل بنجاح :name اهلا",
    /**End RoleController localization */

    /**Start PermissionController localization */
    "all_permissions" => "here are the permissions we can give.",
    "permission_created_successfully" => ".تم انشاء الدور بنجاح :name اهلا",
    "permission_deleted_successfully" => ".تم حذف  الدور بنجاح :name اهلا.",
    "permission_assigned_successfully" => ".تم منح الدور بنجاح :name اهلا",
    "permission_unassigned_successfully" => ".تم حذف الدور من العميل بنجاح :name اهلا",
    /**End PermissionController localization */

    /**Start PermissionRequest localization */
    "permission_name_required" => ".من فضلك ادخل اسم التصريح اولا",
    "permission_slug_required" => "من فضلك ادخل رمز التصريح اولا",
    "permission_id_required" => "من فضلك ادخل رقم التصريح اولا",
    "permission_name_min" => ".اسم التصريح يجب ان يتكون من 3 حروف علي الأقل",
    "permission_id_min" => ".رقم التصريح يجب الا يساوي صفر",
    "permission_slug_min" => ".رمز التصريح يجب ان يتكون من 3 حروف علي الأقل",
    "permission_name_max" => ".اسم التصريح يجب ان يتكون من 191 حروف علي الأكثر",
    "permission_slug_max" => ".رمز التصريح يجب ان يتكون من 191 حروف علي الأكثر",
    "user_id_required" => ".من فضلك ادخل رقم العميل اولا",
    "user_id_min" => ".رقم العميل يجب الا يساوي صفر",
    "permission_already_exists" => ".هذا التصريح موجود بالفعل",
    "permission_slug_regex" => '  "-" رمز التصريح يجب ان لا يحتوي علي ارقام و ان يحتوي علي العلامه',
    "permission_not_exists" => ".هذا التصريح غير موجود",
    "permission_id_not_integer" => ".رقم التصريح يجب ان يكون رقم صحيح",
    "user_id_not_integer" => ".رقم العميل يجب ان يكون رقم صحيح",
    "user_not_exists" => ".هذا العميل غير مسجل لدينا",
    /**End PermissionRequest localization */

    /**Start RoleRequest localization */
    "role_name_required" => ".من فضلك ادخل اسم الدور اولا",
    "role_slug_required" => "من فضلك ادخل رمز الدور اولا",
    "role_id_required" => "من فضلك ادخل رقم الدور اولا",
    "role_name_min" => ".اسم الدور يجب ان يتكون من 3 حروف علي الأقل",
    "role_id_min" =>".رقم الدور يجب الا يساوي صفر",
    "role_slug_min" => ".رمز الدور يجب ان يتكون من 3 حروف علي الأقل",
    "role_name_max" => ".اسم الدور يجب ان يتكون من 191 حروف علي الأكثر",
    "role_slug_max" => ".رمز الدور يجب ان يتكون من 191 حروف علي الأكثر",
    "user_id_required" => ".من فضلك ادخل رقم العميل اولا",
    "user_id_min" => ".رقم العميل يجب الا يساوي صفر",
    "role_already_exists" => ".هذا الدور موجود بالفعل",
    "slug_regex" => '  "-" رمز الدور يجب ان لا يحتوي علي ارقام و ان يحتوي علي العلامه',
    "role_not_exists" => ".هذا الدور غير موجود",
    "role_id_not_integer" =>  ".رقم الدور يجب ان يكون رقم صحيح",
    "user_id_not_integer" => ".رقم العميل يجب ان يكون رقم صحيح",
    "user_not_exists" => ".هذا العميل غير مسجل لدينا",
    /**End RoleRequest localization */

    /**Start UserRequest localization */
    "user_already_exists" => ".هذا العميل موجود بالفعل",
    "not_valid_email" => ".هذا البريد غير صالح",
    "email_min" => ".البريد الالكتروني يجيب ان يتكون من 8 حروف علي الأقل",
    "email_max" => ".البريد الالكتروني يجب ان يتكون من 191 حروف علي الأكثر",
    "name_required" => ".من فضلك ادخل اسم العميل اولا",
    "name_min" => ".اسم العميل يجب ان يتكون من 4 حروف علي الأقل",
    "name_max" => ".اسم العميل يجب ان يتكون من 191 حروف علي الأكثر",
    "password_required" => ".من فضلك ادخل الرقم السري اولا",
    "password_confirmed" => ".من فضلك قم بتأكيد رقم العميل السري اولا",
    "password_min" => ".الرقم السري يجيب ان يتكون من 6 حروف علي الأقل",
    "password_max" => ".الرقم السري يجب ان يتكون من 191 حروف علي الأكثر",
    "password_regex" => "الرقم السري يدب ان يكتوب علي احرف كبيره و صغيره و ارقام و رموز",
    "token_exists" => ".هذه العلامة الرمزية موجودة بالفعل",
    "token_required" => ".العلامة الرمزية مطلوبة ",
    
    /**End UserRequest localization */

    /**Start others */
    "not_supported_language" => ".عذرا هذه اللغة غير مدعومة من تظبيقنا",
    "role_doesnt_have_permission" => 'هذا الدور لا يحتوي علي هذا التصريح',
    "role_already_has_permission" => 'هذا الدور يحتوي بالفعل علي هذا التصريح',
    "user_doesnt_have_permission" => 'هذا العميل لا يحتوي بالفعل علي هذا التصريح',
    "user_doesnt_have_role" =>'هذا العميل لا يحتوي بالفعل علي هذا الدور',
    "user_already_has_permission" =>  'هذا العميل يحتوي بالفعل علي هذا التصريح',
    "user_already_has_role" =>   'هذا العميل يحتوي بالفعل علي هذا الدور',
    /**End others */

];
