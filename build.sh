#!/bin/bash
start=`date +%s`

# START Removing old database volumes -------------------------------------------------------------------

sudo rm -rf app_auth_db_mysql

# END Removing old database volumes -------------------------------------------------------------------

# START Cleaning docker environment --------------------------------------------------------------

docker rm app_auth app_auth_db_mysql app_auth_phpmyadmin -f
docker system prune -f
docker image prune -f
docker container prune -f
docker volume prune -f

# END Cleaning docker environment --------------------------------------------------------------


# START Building Docker containers -------------------------------------------------------------------------

docker-compose up -d

# END Building Docker containers --------------------------------------------------------------


# START Assign permissions to the database volumes --------------------------------------------------------------

sudo chmod 777 -R app_auth_db_mysql
sudo chmod 777 -R app_auth/storage/

# END Assign permissions to the database volumes --------------------------------------------------------------


# START Authentication Microservice commands --------------------------------------------------------------

( while ! docker exec app_auth_db_mysql mysqladmin  --user=root --password=ahmed2012AHMED@\)!@ --host "0.0.0.0" ping --silent &> /dev/null ; do
    echo " ... Waiting for database to be deployed ..." ; sleep 10; done; echo " ... Database has been deployed successfully ..." )


docker exec -t app_auth chmod -R 777 /usr/src/app
docker exec -t app_auth cp .env.example .env
docker exec -t app_auth composer dump-autoload
docker exec -t app_auth composer require "darkaonline/l5-swagger"
docker exec -t app_auth php artisan config:cache
docker exec -t app_auth composer require doctrine/dbal
docker exec -t app_auth php artisan migrate:fresh
docker exec -t app_auth php artisan passport:install --force
docker exec -t app_auth php artisan cache:clear
docker exec -t app_auth php artisan route:cache
docker exec -t app_auth php artisan config:cache

end=`date +%s`

runtime=$((end-start))

echo "Project is successfully deployed in" $runtime "seconds"

docker exec -t app_auth php artisan serve --port=8002


